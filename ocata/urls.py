"""ocata URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from Mails.views import votantes, editar_votante, generar_excel, funciones_magicas as magic

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', votantes, name='votantes'),
    url(r'^generar-excel/', generar_excel, name='excel'),
    url(r'^editar/(?P<pk>\w+)/', editar_votante, name='editar'),
    url(r'^change/', magic, name='magia')
]
