from Mails.models import Votante
from django import forms


class VotanteForm(forms.ModelForm):

    class Meta:
        model = Votante
        fields = ('nombre', 'cedula', 'codigo')

