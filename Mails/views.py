from io import BytesIO
from django.core.files.base import ContentFile
from django.http import StreamingHttpResponse
from django.shortcuts import render, redirect
import xlwt
from Mails.fitsheet import FitSheetWrapper
from Mails.forms import VotanteForm
from .models import Votante, ServidorActual
from .main_report import ReportAll
# Create your views here.


def funciones_magicas(request):
    #Votante.set_all()
    Votante.emails_server_set_all()
    return redirect('votantes')


def votantes(request):
    form = VotanteForm()
    if request.POST:
        form = VotanteForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('votantes')
    context = {
        'votantes': Votante.objects.all().order_by('-id'),
        'form': form,
        'contador': Votante.objects.count(),
        'mail_': ServidorActual.objects.all()[:1].get()
    }
    return render(request, "main.html", context)


def editar_votante(request, **kwargs):
    pk = kwargs.get('pk')
    votante = Votante.objects.get(id=pk)
    form = VotanteForm(instance=votante)
    if request.POST:
        form = VotanteForm(request.POST, instance=votante)
        if form.is_valid():
            form.save()
            return redirect('votantes')
    context = {
        'votantes': Votante.objects.all().order_by('-id'),
        'form': form,
        'contador': Votante.objects.count()
    }
    return render(request, "main.html", context)


def generar_excel(request):
    report = ReportAll()
    query = report.query
    archivo = report.file_name
    workbook = xlwt.Workbook(encoding='utf8')
    worksheet = workbook.add_sheet("votantes")
    row_num = 0
    worksheet = FitSheetWrapper(worksheet)
    columns = ['id', 'nombre', 'cedula', 'codigo', 'correo']

    for col_num in range(len(columns)):
        worksheet.write(row_num, col_num, columns[col_num])

    for votante in query:
        row_num += 1
        id = votante.id
        nombre = votante.nombre
        cedula = votante.cedula
        codigo = votante.codigo
        correo = votante.mail
        row = [id, nombre, cedula, codigo, correo]
        for col_num in range(len(row)):
            worksheet.write(row_num, col_num, row[col_num])

    file = BytesIO()
    workbook.save(file)
    file.seek(0)
    response = StreamingHttpResponse(streaming_content=ContentFile(file.getvalue()),
                                     content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename={}'.format(archivo)
    file.close()
    return response

