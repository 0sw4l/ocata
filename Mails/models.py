from __future__ import unicode_literals

from django.db import models


# Create your models here.


class ServidorMail(models.Model):

    nombre = models.CharField(max_length=50, unique=True)
    url_email_server = models.URLField(unique=True)
    direccion = models.CharField(max_length=50, unique=True)
    color = models.CharField(max_length=10, default='FF00FF')

    class Meta:
        verbose_name_plural = 'Servidores de Email'
        verbose_name = 'Servidor de Email'
        unique_together = ['nombre', 'url_email_server']

    def __unicode__(self):
        return '{0} - URL : {1}'.format(self.nombre, self.url_email_server)

    def save(self, *args, **kwargs):
        self.color = "#{0}".format(self.color)
        super(ServidorMail, self).save(*args, **kwargs)


class Votante(models.Model):
    nombre = models.CharField(max_length=50)
    cedula = models.IntegerField(unique=True)
    codigo = models.CharField(max_length=20, blank=True, null=True)
    mail = models.EmailField(default='')
    servidor = models.ForeignKey(ServidorMail, editable=False, blank=True, null=True)

    def save(self, *args, **kwargs):
        if not self.id:
            mail_ = ServidorActual.objects.all()[:1].get()
            self.servidor = mail_.servidor
            self.mail = '{0}-{1}{2}'.format(self.nombre, self.cedula, mail_.servidor.direccion)
        super(Votante, self).save(*args, **kwargs)

    @staticmethod
    def set_all():
        mail_ = ServidorActual.objects.all()[:1].get()
        for votante in Votante.objects.all():
            votante.mail = '{0}-{1}{2}'.format(votante.nombre, votante.cedula, mail_.servidor.direccion)
            votante.save()

    @staticmethod
    def emails_server_set_all():
        server = 0
        for votante in Votante.objects.all():
            mail = votante.mail
            mail = str(mail)
            if mail.find('mailinator') >= 0:
                server = ServidorMail.objects.get(id=1)
                votante.servidor = server

            if mail.find('yopmail') >= 0:
                server = ServidorMail.objects.get(id=2)
            votante.servidor = server
            votante.save()


class ServidorActual(models.Model):
    servidor = models.ForeignKey(ServidorMail)

    class Meta:
        verbose_name_plural = 'Cambiar Servidor Actual'
        verbose_name = 'Servidor Actual'
        unique_together = ['servidor']

    def __unicode__(self):
        return "{}".format(self.servidor)
