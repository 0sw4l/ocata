from django.contrib import admin
from .models import Votante, ServidorActual, ServidorMail
# Register your models here.


@admin.register(Votante)
class VotanteAdmin(admin.ModelAdmin):

    list_display = ['id', 'nombre', 'mail', 'cedula', 'codigo']
    search_fields = ['id', 'nombre', 'mail', 'cedula', 'codigo']
    ordering = ['id', 'nombre', 'mail', 'cedula', 'codigo']


@admin.register(ServidorMail)
class ServidorMailAdmin(admin.ModelAdmin):

    list_display = ['id', 'nombre', 'url_email_server']
    search_fields = ['id', 'nombre', 'url_email_server']
    ordering = ['id', 'nombre', 'url_email_server']


@admin.register(ServidorActual)
class ServidorActualAdmin(admin.ModelAdmin):

    list_display = ['servidor']
    search_fields = ['id', 'servidor']
    ordering = ['id', 'servidor']

    def has_add_permission(self, request):
        count = ServidorActual.objects.count()
        if count == 0:
            return True
        return False
