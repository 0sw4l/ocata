# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-16 15:38
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Mails', '0003_auto_20160415_1734'),
    ]

    operations = [
        migrations.CreateModel(
            name='ServidorActual',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
            options={
                'verbose_name': 'Servidor Actual',
                'verbose_name_plural': 'Cambiar Servidor Actual',
            },
        ),
        migrations.CreateModel(
            name='ServidorMail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=50, unique=True)),
                ('url_email_server', models.URLField(unique=True)),
            ],
            options={
                'verbose_name': 'Servidor de Email',
                'verbose_name_plural': 'Servidores de Email',
            },
        ),
        migrations.AlterUniqueTogether(
            name='servidormail',
            unique_together=set([('nombre', 'url_email_server')]),
        ),
        migrations.AddField(
            model_name='servidoractual',
            name='servidor',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='Mails.ServidorMail'),
        ),
        migrations.AlterUniqueTogether(
            name='servidoractual',
            unique_together=set([('servidor',)]),
        ),
    ]
