# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-15 22:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Mails', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='votante',
            name='codigo',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
    ]
