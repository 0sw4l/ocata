from .models import Votante


class ReportMain(object):

    _file_name = 'votantes {}.xls'

    def __init__(self):
        self.query = Votante.objects.all()

    @property
    def file_name(self):
        pass


class ReportAll(ReportMain):

    @property
    def file_name(self):
        return self._file_name.format("de catalino sosa")